package de.hdc.math

/**
 * Cross-multiplication (German: Dreisatz)
 * Solves the equation a0/b0 = a1/b1 for any one of the parameters being null.
 *
 * @throws IllegalArgumentException if there is more or less than 1 parameter set to null.
 */
public fun dreisatz(
    a0: Number?,
    b0: Number?,
    a1: Number?,
    b1: Number?
): Double {
    var nullParms = 0
    if (a0 == null) nullParms += 1
    if (b0 == null) nullParms += 1
    if (a1 == null) nullParms += 1
    if (b1 == null) nullParms += 1
    require(nullParms == 1) { "Found $nullParms missing parameters. Exactly one missing parameter allowed!" }

    return when (true) {
        (a0 == null) -> a1!!.toDouble() / b1!!.toDouble() * b0!!.toDouble()
        (b0 == null) -> b1!!.toDouble() / a1!!.toDouble() * a0.toDouble()
        (a1 == null) -> a0.toDouble() / b0.toDouble() * b1!!.toDouble()
        // (b1 == null)
        else -> b0.toDouble() / a0.toDouble() * a1.toDouble()
    }
}

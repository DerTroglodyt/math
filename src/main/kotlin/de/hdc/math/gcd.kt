package de.hdc.math

/**
 * Calculates the Greatest Common Divisor of a list of numbers.
 */
public fun gcd(numbers: List<Long>): Long {
    require(numbers.isNotEmpty()) { "List must not be empty!" }

    var result = numbers[0]
    (1..<numbers.size).forEach { i ->
        var num1 = result
        var num2 = numbers[i]
        while (num2 != 0L) {
            val temp = num2
            num2 = num1 % num2
            num1 = temp
        }
        result = num1
    }
    return result
}

package de.hdc.math

/**
 * PID Controller.
 */
public class PidController(
    /**
     * Proportional gain.
     */
    public var p: Double,
    /**
     * Integral gain.
     */
    public var i: Double,
    /**
     * Derivative gain.
     */
    public var d: Double,
    /**
     * Count of ticks to remember for integral gain.
     */
    historyCount: Int
) {
    private class Ring(size: Int, init: () -> Double) {
        private val values: Array<Double> = Array(size) { init() }
        private var pos = 0

        fun push(x: Double) {
            values[pos] = x
            pos = (pos + 1) % values.size
        }

        fun sum(): Double = values.sum()
    }

    private var lastError = 0.0
    private var sumError = Ring(historyCount) { 0.0 }

    /**
     * Calculates the controller output.
     * @param seconds Elapsed time since last invocation of this method.
     * @param actual The measured value of the system.
     * @param aim The desired value of the system.
     */
    public fun tick(seconds: Double, actual: Double, aim: Double): Double {
        val error = aim - actual
        sumError.push(error)
        val prop = error * p
        val integr = sumError.sum() * i
        val deriv = (lastError - error) * d
        lastError = error
        return (prop + integr) * seconds + deriv
    }
}

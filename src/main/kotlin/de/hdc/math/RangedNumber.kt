package de.hdc.math

import kotlin.reflect.KProperty

/**
 * Holds a value which is guarantied to lie in a fixed range.
 * Can be used as a delegate:
 *   val d: Double by RangedDouble(0.0)
 */
public interface RangedNumber<T> : Comparable<RangedNumber<T>>
        where T : Number, T : Comparable<T> {
    /** The actual value represented by this class. */
    public val value: T
    public operator fun getValue(
        thisRef: Any?,
        property: KProperty<*>
    ): T

    public operator fun unaryMinus(): RangedNumber<T>

    public operator fun unaryPlus(): RangedNumber<T>

    /** Adds the values of two RangeNumbers and clamp them to the range of this RangeNumber. */
    public operator fun plus(x: Number): RangedNumber<T>

    /** Subtracts the values of two RangeNumbers and clamp them to the range of this RangeNumber. */
    public operator fun minus(x: Number): RangedNumber<T>

    /** Multiplies the values of two RangeNumbers and clamp them to the range of this RangeNumber. */
    public operator fun times(x: Number): RangedNumber<T>

    /** Divides the values of two RangeNumbers and clamp them to the range of this RangeNumber. */
    public operator fun div(x: Number): RangedNumber<T>

    /** Calculates the remainder of the values of two RangeNumbers and clamp them to the range of this RangeNumber. */
    public operator fun rem(x: Number): RangedNumber<T>
}

/**
 * Holds a value which is guarantied to lie in a fixed range.
 * Can be used as a delegate:
 *   val d: Double by RangedDouble(0.0)
 */
public abstract class AbstractRangedNumber<T>(
    initialValue: T,
    /** The inclusive interval in which the value is guarantied to lie. */
    public val range: ClosedRange<T>
) : RangedNumber<T>, Number(), Comparable<RangedNumber<T>>
        where T : Number, T : Comparable<T> {
    override val value: T = initialValue.clamp(range)

    override operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value

    override operator fun unaryPlus(): RangedNumber<T> = this

    override fun toByte(): Byte = value.toInt().toByte()
    @Deprecated(
        "Direct conversion to Char is deprecated. Use toInt().toChar() or Char constructor instead.\nIf you override toChar() function in your Number inheritor, it's recommended to gradually deprecate the overriding function and then remove it.\nSee https://youtrack.jetbrains.com/issue/KT-46465 for details about the migration",
        replaceWith = ReplaceWith("this.toInt().toChar()")
    )
    override fun toChar(): Char = value.toInt().toChar()
    override fun toDouble(): Double = value.toDouble()
    override fun toFloat(): Float = value.toFloat()
    override fun toInt(): Int = value.toInt()
    override fun toLong(): Long = value.toLong()
    override fun toShort(): Short = value.toLong().toShort()

    override fun compareTo(other: RangedNumber<T>): Int = value.compareTo(other.value)
    override fun hashCode(): Int = value.hashCode()
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RangedNumber<*>

        return value == other.value
    }
}

/**
 * Holds a value which is guarantied to lie in a fixed range.
 */
public class RangedFloat(initialValue: Float, range: ClosedRange<Float>) :
    AbstractRangedNumber<Float>(initialValue, range) {
    override operator fun unaryMinus(): RangedNumber<Float> = times(-1f)
    override fun plus(x: Number): RangedNumber<Float> = RangedFloat(value + x.toFloat(), range)
    override fun minus(x: Number): RangedNumber<Float> = RangedFloat(value - x.toFloat(), range)
    override fun times(x: Number): RangedNumber<Float> = RangedFloat(value * x.toFloat(), range)
    override fun div(x: Number): RangedNumber<Float> = RangedFloat(value / x.toFloat(), range)
    override fun rem(x: Number): RangedNumber<Float> = RangedFloat(value % x.toFloat(), range)
}

/**
 * Holds a value which is guarantied to lie in a fixed range.
 */
public class RangedDouble(initialValue: Double, range: ClosedRange<Double>) :
    AbstractRangedNumber<Double>(initialValue, range) {
    override operator fun unaryMinus(): RangedNumber<Double> = times(-1.0)
    override fun plus(x: Number): RangedNumber<Double> = RangedDouble(value + x.toDouble(), range)
    override fun minus(x: Number): RangedNumber<Double> = RangedDouble(value - x.toDouble(), range)
    override fun times(x: Number): RangedNumber<Double> = RangedDouble(value * x.toDouble(), range)
    override fun div(x: Number): RangedNumber<Double> = RangedDouble(value / x.toDouble(), range)
    override fun rem(x: Number): RangedNumber<Double> = RangedDouble(value % x.toDouble(), range)
}

/**
 * Holds a value which is guarantied to lie in a fixed range.
 */
public class RangedShort(initialValue: Short, range: ClosedRange<Short>) :
    AbstractRangedNumber<Short>(initialValue, range) {
    override operator fun unaryMinus(): RangedNumber<Short> = times(-1)
    override fun plus(x: Number): RangedNumber<Short> = RangedShort((value + x.toShort()).toShort(), range)
    override fun minus(x: Number): RangedNumber<Short> = RangedShort((value - x.toShort()).toShort(), range)
    override fun times(x: Number): RangedNumber<Short> = RangedShort((value * x.toShort()).toShort(), range)
    override fun div(x: Number): RangedNumber<Short> = RangedShort((value / x.toShort()).toShort(), range)
    override fun rem(x: Number): RangedNumber<Short> = RangedShort((value % x.toShort()).toShort(), range)
}

/**
 * Holds a value which is guarantied to lie in a fixed range.
 */
public class RangedInt(initialValue: Int, range: ClosedRange<Int>) :
    AbstractRangedNumber<Int>(initialValue, range) {
    override operator fun unaryMinus(): RangedNumber<Int> = times(-1)
    override fun plus(x: Number): RangedNumber<Int> = RangedInt(value + x.toInt(), range)
    override fun minus(x: Number): RangedNumber<Int> = RangedInt(value - x.toInt(), range)
    override fun times(x: Number): RangedNumber<Int> = RangedInt(value * x.toInt(), range)
    override fun div(x: Number): RangedNumber<Int> = RangedInt(value / x.toInt(), range)
    override fun rem(x: Number): RangedNumber<Int> = RangedInt(value % x.toInt(), range)
}

/**
 * Holds a value which is guarantied to lie in a fixed range.
 */
public class RangedLong(initialValue: Long, range: ClosedRange<Long>) :
    AbstractRangedNumber<Long>(initialValue, range) {
    override operator fun unaryMinus(): RangedNumber<Long> = times(-1L)
    override fun plus(x: Number): RangedNumber<Long> = RangedLong(value + x.toLong(), range)
    override fun minus(x: Number): RangedNumber<Long> = RangedLong(value - x.toLong(), range)
    override fun times(x: Number): RangedNumber<Long> = RangedLong(value * x.toLong(), range)
    override fun div(x: Number): RangedNumber<Long> = RangedLong(value / x.toLong(), range)
    override fun rem(x: Number): RangedNumber<Long> = RangedLong(value % x.toLong(), range)
}

package de.hdc.math

import kotlin.math.*

public sealed class RootsResult(public val result: Pair<Number, Number>)
public class RealRoots(r: Double, s: Double) : RootsResult(r to s)
public class ComplexRoots(r: ComplexNumber, s: ComplexNumber) : RootsResult(r to s)

/**
 * Find the roots for: ax² + bx + c
 */
public fun roots(a: Double, b: Double, c: Double): RootsResult {
    val bb = b / a
    val cc = c / a
    val m = -bb / 2
    var d = sqrt(m * m - cc)
    if (d.isNaN()) {
        d = m * m - cc
        return ComplexRoots(ComplexNumber(m, d), ComplexNumber(m, -d))
    }
    return RealRoots(m - d, m + d)
}

/**
 * Find the roots for: ax² + bx + c
 */
public fun roots(a: Int, b: Int, c: Int): RootsResult {
    return roots(a.toDouble(), b.toDouble(), c.toDouble())
}

package de.hdc.math

import kotlin.math.*

/**
 * Constructs a ComplexNumber via infix Notation.
 * Attention: The given [Number]s are converted to [Double]s so precision loss is possible!
 */
public infix fun Number.i(x: Number): ComplexNumber = ComplexNumber(this.toDouble(), x.toDouble())

/**
 * Represents a value in the complex number plane.
 * Internal representation has [Double] precision.
 */
public data class ComplexNumber(val r: Double, val i: Double) : Number(), Comparable<ComplexNumber> {
    /**
     * The length of this complex vector in the plane.
     */
    val abs: Double by lazy { hypot(r, i) }

    public operator fun plus(other: ComplexNumber): ComplexNumber = ComplexNumber(r + other.r, i + other.i)

    public operator fun minus(other: ComplexNumber): ComplexNumber = ComplexNumber(r - other.r, i - other.i)

    public operator fun times(other: ComplexNumber): ComplexNumber =
        ComplexNumber(r * other.r - i * other.i, r * other.i + i * other.r)

    public operator fun div(other: ComplexNumber): ComplexNumber =
        this * other.conjugate() / other.abs.pow(2.0)

    public operator fun times(factor: Number): ComplexNumber =
        ComplexNumber(factor.toDouble() * r, factor.toDouble() * i)

    public operator fun div(factor: Number): ComplexNumber =
        ComplexNumber(r / factor.toDouble(), i / factor.toDouble())

    public fun conjugate(): ComplexNumber = ComplexNumber(r, -1 * i)

    public fun exponential(): ComplexNumber = ComplexNumber(cos(i), sin(i)) * exp(r)

    override fun compareTo(other: ComplexNumber): Int {
        return when {
            (r == other.r) && (i == other.i) -> 0
            abs > other.abs -> 1
            else -> -1
        }
    }

    override fun toString(): String {
        return if (i > 0) "($r + ${i}i)" else "($r - ${abs(i)}i)"
    }

    @Deprecated("Complex number can not be converted to Byte!", level = DeprecationLevel.ERROR)
    override fun toByte(): Byte {
        throw IllegalAccessException("Complex number can not be converted to Byte!")
    }

    @Deprecated("Complex number can not be converted to Double!", level = DeprecationLevel.ERROR)
    override fun toDouble(): Double {
        throw IllegalAccessException("Complex number can not be converted to Double!")
    }

    @Deprecated("Complex number can not be converted to Char!", level = DeprecationLevel.ERROR)
    override fun toChar(): Char {
        throw IllegalAccessException("Complex number can not be converted to Char!")
    }

    @Deprecated("Complex number can not be converted to Float!", level = DeprecationLevel.ERROR)
    override fun toFloat(): Float {
        throw IllegalAccessException("Complex number can not be converted to Float!")
    }

    @Deprecated("Complex number can not be converted to Int!", level = DeprecationLevel.ERROR)
    override fun toInt(): Int {
        throw IllegalAccessException("Complex number can not be converted to Int!")
    }

    @Deprecated("Complex number can not be converted to Long!", level = DeprecationLevel.ERROR)
    override fun toLong(): Long {
        throw IllegalAccessException("Complex number can not be converted to Long!")
    }

    @Deprecated("Complex number can not be converted to Short!", level = DeprecationLevel.ERROR)
    override fun toShort(): Short {
        throw IllegalAccessException("Complex number can not be converted to Short!")
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ComplexNumber
        if (r != other.r) return false
        return i == other.i
    }

    override fun hashCode(): Int {
        var result = r.hashCode()
        result = 31 * result + i.hashCode()
        return result
    }

    /**
     * Projects a value from one scale onto another.
     * The value may lie outside the given from range and thus get mapped outside the given to range!
     */
    public fun ComplexNumber.mapTo(
        from: ClosedFloatingPointRange<ComplexNumber>,
        to: ClosedFloatingPointRange<ComplexNumber>
    ): ComplexNumber {
        require(from.start != from.endInclusive) { "From range width must be greater than zero!" }
        val slope = (to.endInclusive - to.start) / (from.endInclusive - from.start)
        return to.start + (this - from.start) * slope
    }
}

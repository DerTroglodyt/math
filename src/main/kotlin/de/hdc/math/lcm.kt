package de.hdc.math

public fun lcm(a: Long, b: Long): Long {
    val larger = if (a > b) a else b
    val maxLcm = a * b
    var lcm = larger
    while (lcm <= maxLcm) {
        if (lcm % a == 0L && lcm % b == 0L) {
            return lcm
        }
        lcm += larger
    }
    return maxLcm
}

public fun lcm(numbers: List<Long>): Long {
    var result = numbers[0]
    (1..<numbers.size).forEach { i ->
        result = lcm(result, numbers[i])
    }
    return result
}

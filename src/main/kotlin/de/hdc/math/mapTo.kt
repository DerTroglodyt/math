@file:Suppress("HardCodedStringLiteral")

package de.hdc.math

import java.math.BigDecimal
import java.math.BigInteger
import kotlin.math.roundToInt
import kotlin.math.roundToLong

private const val INVALID_RANGE = "From range width must be greater than zero!"

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun Float.mapTo(from: ClosedFloatingPointRange<Float>, to: ClosedFloatingPointRange<Float>): Float {
    require(from.start != from.endInclusive) { INVALID_RANGE }
    val slope = (to.endInclusive - to.start) / (from.endInclusive - from.start)
    return to.start + (this - from.start) * slope
}

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun Double.mapTo(from: ClosedFloatingPointRange<Double>, to: ClosedFloatingPointRange<Double>): Double {
    require(from.start != from.endInclusive) { INVALID_RANGE }
    val slope = (to.endInclusive - to.start) / (from.endInclusive - from.start)
    return to.start + (this - from.start) * slope
}

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun Int.mapTo(from: IntRange, to: IntRange): Int {
    require(from.first != from.last) { INVALID_RANGE }
    val slope = (to.last - to.first).toDouble() / (from.last - from.first).toDouble()
    return (to.first + (this - from.first) * slope).roundToInt()
}

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun Long.mapTo(from: LongRange, to: LongRange): Long {
    require(from.first != from.last) { INVALID_RANGE }
    val slope = (to.last - to.first).toDouble() / (from.last - from.first).toDouble()
    return (to.first + (this - from.first) * slope).roundToLong()
}

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun UInt.mapTo(from: UIntRange, to: UIntRange): UInt {
    require(from.first != from.last) { INVALID_RANGE }
    val slope = (to.last - to.first).toDouble() / (from.last - from.first).toDouble()
    return ((to.first + (this - from.first)).toDouble() * slope).roundToInt().toUInt()
}

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun ULong.mapTo(from: ULongRange, to: ULongRange): ULong {
    require(from.first != from.last) { INVALID_RANGE }
    val slope = (to.last - to.first).toDouble() / (from.last - from.first).toDouble()
    return ((to.first + (this - from.first)).toDouble() * slope).roundToLong().toULong()
}

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun BigInteger.mapTo(from: ClosedRange<BigInteger>, to: ClosedRange<BigInteger>): BigInteger {
    require(from.start != from.endInclusive) { INVALID_RANGE }
    val slope = to.endInclusive.subtract(to.start).divide(from.endInclusive.subtract(from.start))
    return to.start.add(this.subtract(from.start).multiply(slope))
}

/**
 * Projects a value from one scale onto another.
 * The value may lie outside the given from range and thus get mapped outside the given to range!
 */
public fun BigDecimal.mapTo(from: ClosedRange<BigDecimal>, to: ClosedRange<BigDecimal>): BigDecimal {
    require(from.start != from.endInclusive) { INVALID_RANGE }
    val slope = to.endInclusive.subtract(to.start).divide(from.endInclusive.subtract(from.start))
    return to.start.add(this.subtract(from.start).multiply(slope))
}

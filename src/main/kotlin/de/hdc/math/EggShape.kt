package de.hdc.math

import kotlin.math.*

/**
 * Produces 2*count points that lie on the 2D shell of an egg.
 * x: -length/2 .. length/2
 * y: -maxWidth/2 .. maxWidth/2
 *
 *                    y
 *                    |
 *               .    |
 *        .      |    |     .---------------- quarterLenWidth
 *    .          |    |     |    .
 *  .            |    |     |        .
 * .-------------|----+-----+----x    . ----
 *  .           -w    |    L/4       .
 *    .               |          .
 *        .           |     .
 *               .    |
 *                    |
 */
public fun eggShell(
    count: Int,
    length: Double,
    maxWidth: Double,
    lengthToMaxWidth: Double,
    quarterLenWidth: Double
): List<Pair<Double, Double>> {
    val list = mutableListOf<Pair<Double, Double>>()
    repeat(count) {
        val x = (length / count) * it - (length / 2.0)
        val y = eggShellHalve(x, length, maxWidth, lengthToMaxWidth, quarterLenWidth)
        list.add(x to y)
    }
    return list.map { (x, y) -> x to y } +
            list.reversed().map { (x, y) -> x to -y }
}

/**
 * Produces one half of an egg shell for -L/2 <= x <= L/2
 *
 *                    y
 *                    |
 *               .    |
 *        .      |    |     .---------------- quarterLenWidth
 *    .          |    |     |    .
 *  .            |    |     |        .
 * .-------------|----+-----+----x    . ----
 *  .           -w    |    L/4       .
 *    .               |          .
 *        .           |     .
 *               .    |
 *                    |
 */
public fun eggShellHalve(
    x: Double,
    length: Double,
    maxWidth: Double,
    lengthToMaxWidth: Double,
    quarterLenWidth: Double
): Double {
    check(x >= length / -2.0) { "x ($x) can not be < -length/2!" }
    check(x <= length / 2.0) { "x ($x) can not be > length/2!" }
    check(abs(lengthToMaxWidth) >= 0.0) { "|lengthToMaxWidth| ($lengthToMaxWidth) must be >= 0!" }
    check(abs(lengthToMaxWidth) < length) { "|lengthToMaxWidth| ($lengthToMaxWidth) can not be >= length!" }
    check(quarterLenWidth > 0.0) { "quarterLenWidth ($quarterLenWidth) must be > 0!" }
    check(quarterLenWidth < maxWidth) { "quarterLenWidth ($quarterLenWidth) can not be > maxWidth!" }
    val x2 = x * x
    val L = length
    val L2 = L * L
    val L3 = L * L * L
    val B = maxWidth
    val D = quarterLenWidth
    val w = abs(lengthToMaxWidth)
    val w2 = w * w
    return B / 2.0 *
            sqrt((L2 - 4.0 * x2) / (L2 + 8.0 * w * x + 4.0 * w2)) *
            (1.0 -
                    (sqrt(5.5 * L2 + 11.0 * L * w + 4.0 * w2)) *
                    (sqrt(3.0) * B * L - 2.0 * D * sqrt(L2 + 2.0 * w * L + 4.0 * w2)) /
                    (sqrt(3.0) * B * L * (sqrt(5.5 * L2 + 11.0 * L * w + 4.0 * w2) -
                            2.0 * sqrt(L2 + 2.0 * w * L + 4.0 * w2))
                            ) *
                    (1.0 -
                            sqrt(
                                (L * (L2 + 8.0 * w * x + 4.0 * w2)) /
                                        (2.0 * (L - 2.0 * w) * x2 +
                                                (L2 + 8.0 * L * w - 4.0 * w2) * x +
                                                2.0 * L * w2 + L2 * w + L3)
                            )
                            )
                    )
}

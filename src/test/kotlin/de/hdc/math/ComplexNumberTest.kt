package de.hdc.math

import io.kotest.assertions.throwables.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import io.kotest.matchers.doubles.*
import kotlin.math.*

class ComplexNumberTest : FreeSpec() {
    init {
        "creation" {
            1 i 5 shouldBe ComplexNumber(1.0, 5.0)
            1.0 i 5.0 shouldBe ComplexNumber(1.0, 5.0)

            ComplexNumber(1.0, 5.0).r shouldBe 1
            ComplexNumber(1.0, 5.0).i shouldBe 5
        }

        "toString()" {
            (1 i 5).toString() shouldBe "(1.0 + 5.0i)"
            (-1 i 5).toString() shouldBe "(-1.0 + 5.0i)"
            (1 i -5).toString() shouldBe "(1.0 - 5.0i)"
            (-1 i -5).toString() shouldBe "(-1.0 - 5.0i)"
        }

        "simple arithmetic" {
            shouldThrow<IllegalAccessException> {
                (1 i 5 + 2 i 8) shouldBe (3 i 13)
            }
            ((1 i 5) + (2 i 8)) shouldBe (3 i 13)
            ((1 i 5) - (2 i 8)) shouldBe (-1 i -3)
            ((1 i 5) * (2 i 8)) shouldBe (-38 i 18)

            (3 i 2) + (5 i 5) shouldBe (8 i 7)
            (5 i 5) - (3 i 2) shouldBe (2 i 3)
            (3 i 5) * (4 i 11) shouldBe (-43 i 53)
            val t = (2.0 i 5.0) / (3.0 i 7.0)
            val exp = ((41.0 / 58.0) i (1.0 / 58.0))
            (t.r - exp.r) shouldBe (0.0 plusOrMinus 0.000000000000001)
            (t.i - exp.i) shouldBe (0.0 plusOrMinus 0.000000000000001)

            (3 i 2) * 2 shouldBe (6 i 4)
            (6 i 4) / 2 shouldBe (3 i 2)

            (3 i 2) * 2.0 shouldBe (6 i 4)
            (6 i 4) / 2.0 shouldBe (3 i 2)
        }

        "func" {
            (4 i 3).abs shouldBe 5.0
            (239 i 1).abs shouldBe (169.0 * sqrt(2.0)).plusOrMinus(0.0000000000001)

            (11 i 39).conjugate() shouldBe (11 i -39)

            // todo
            (2 i 3).exponential()
        }
    }
}

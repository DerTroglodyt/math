package de.hdc.math

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.*
import io.kotest.matchers.types.*
import kotlin.math.sqrt

class QuadraticFormularTest : FreeSpec() {
    init {
        "Result types" {
            roots(1.0, 10.0, 3.0).shouldBeInstanceOf<RealRoots>()
            roots(1.0, 10.0, 3.0).shouldNotBeInstanceOf<ComplexRoots>()
            roots(1.0, -6.0, 10.0).shouldBeInstanceOf<ComplexRoots>()
        }

        "Bsp Double" {
            roots(1.0, 10.0, 3.0).result shouldBe (-5.0 - sqrt(22.0) to -5.0 + sqrt(22.0))
            roots(1.0, -7.0, 12.0).result shouldBe (3.0 to 4.0)

            roots(1.0, -6.0, 10.0).result shouldBe Pair(3.0 i -1.0, 3.0 i 1.0)
            roots(3.0, -4.0, 5.0).result shouldBe Pair(
                (2.0 / 3.0) i (-11.0 / 9.0),
                (2.0 / 3.0) i (11.0 / 9.0)
            )
        }

        "Bsp Int" {
            roots(1, 10, 3).result shouldBe Pair(-5.0 - sqrt(22.0), -5.0 + sqrt(22.0))
            roots(1, -7, 12).result shouldBe Pair(3.0, 4.0)

            roots(1, -6, 10).result shouldBe Pair(3 i -1, 3 i 1)
            roots(3, -4, 5).result shouldBe Pair((2.0 / 3.0) i (-11.0 / 9.0), (2.0 / 3.0) i (11.0 / 9.0))
        }
    }
}

package de.hdc.math

import io.kotest.core.spec.style.FreeSpec
import java.util.*
import kotlin.math.*

/**
 * FFT and convolution test (Java)
 *
 * Copyright (c) 2020 Project Nayuki. (MIT License)
 * https://www.nayuki.io/page/free-small-fft-in-multiple-languages
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * - The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 * - The Software is provided "as is", without warranty of any kind, express or
 *   implied, including but not limited to the warranties of merchantability,
 *   fitness for a particular purpose and noninfringement. In no event shall the
 *   authors or copyright holders be liable for any claim, damages or other
 *   liability, whether in an action of contract, tort or otherwise, arising from,
 *   out of or in connection with the Software or the use or other dealings in the
 *   Software.
 */
class FftTest : FreeSpec() {
    init {
        "Main test functions" - {
            "Test power-of-2 size FFTs" {
                for (i in 0..12) testFft(1 shl i)
            }

            "Test small size FFTs" {
                for (i in 0..29) testFft(i)
            }

            "Test diverse size FFTs" {
                run {
                    var i = 0
                    var prev = 0
                    while (i <= 100) {
                        val n = 1500.0.pow(i / 100.0).roundToInt()
                        if (n > prev) {
                            testFft(n)
                            prev = n
                        }
                        i++
                    }
                }
            }

            "Test power-of-2 size convolutions" {
                for (i in 0..12) testConvolution(1 shl i)
            }

            "Test diverse size convolutions" {
                var i = 0
                var prev = 0
                while (i <= 100) {
                    val n = 1500.0.pow(i / 100.0).roundToInt()
                    if (n > prev) {
                        testConvolution(n)
                        prev = n
                    }
                    i++
                }
                println()
                System.out.printf("Max log err = %.1f%n", maxLogError)
                println("Test " + if (maxLogError < -10) "passed" else "failed")
            }
        }
    }

    private fun testFft(size: Int) {
        val inputreal = randomReals(size)
        val inputimag = randomReals(size)
        val expectreal = DoubleArray(size)
        val expectimag = DoubleArray(size)
        naiveDft(inputreal, inputimag, expectreal, expectimag, false)

        val actualreal = inputreal.clone()
        val actualimag = inputimag.clone()
        Fft.transform(actualreal, actualimag)

        var err = log10RmsErr(expectreal, expectimag, actualreal, actualimag)
        for (i in 0 until size) {
            actualreal[i] /= size.toDouble()
            actualimag[i] /= size.toDouble()
        }
        Fft.inverseTransform(actualreal, actualimag)
        err = max(log10RmsErr(inputreal, inputimag, actualreal, actualimag), err)
        System.out.printf("fftsize=%4d  logerr=%5.1f%n", size, err)
    }

    private fun testConvolution(size: Int) {
        val input0real = randomReals(size)
        val input0imag = randomReals(size)
        val input1real = randomReals(size)
        val input1imag = randomReals(size)
        val expectreal = DoubleArray(size)
        val expectimag = DoubleArray(size)
        naiveConvolve(input0real, input0imag, input1real, input1imag, expectreal, expectimag)

        val actualreal = DoubleArray(size)
        val actualimag = DoubleArray(size)
        Fft.convolve(input0real, input0imag, input1real, input1imag, actualreal, actualimag)

        System.out.printf(
            "convsize=%4d  logerr=%5.1f%n", size,
            log10RmsErr(expectreal, expectimag, actualreal, actualimag)
        )
    }

    /*---- Naive reference computation functions ----*/
    private fun naiveDft(
        inreal: DoubleArray,
        inimag: DoubleArray,
        outreal: DoubleArray,
        outimag: DoubleArray,
        inverse: Boolean
    ) {
        val n = inreal.size
        require(n == inimag.size && n == outreal.size && n == outimag.size) { "Mismatched lengths" }
        val coef = (if (inverse) 2 else -2) * Math.PI
        for (k in 0 until n) {  // For each output element
            var sumreal = 0.0
            var sumimag = 0.0
            for (t in 0 until n) {  // For each input element
                val angle = coef * (t.toLong() * k % n).toInt() / n // This is more accurate than t * k
                sumreal += inreal[t] * cos(angle) - inimag[t] * sin(angle)
                sumimag += inreal[t] * sin(angle) + inimag[t] * cos(angle)
            }
            outreal[k] = sumreal
            outimag[k] = sumimag
        }
    }

    private fun naiveConvolve(
        xreal: DoubleArray,
        ximag: DoubleArray,
        yreal: DoubleArray,
        yimag: DoubleArray,
        outreal: DoubleArray,
        outimag: DoubleArray
    ) {
        val n = xreal.size
        require(n == ximag.size && n == yreal.size && n == yimag.size && n == outreal.size && n == outimag.size) { "Mismatched lengths" }
        Arrays.fill(outreal, 0.0)
        Arrays.fill(outimag, 0.0)
        for (i in 0 until n) {
            for (j in 0 until n) {
                val k = (i + j) % n
                outreal[k] += xreal[i] * yreal[j] - ximag[i] * yimag[j]
                outimag[k] += xreal[i] * yimag[j] + ximag[i] * yreal[j]
            }
        }
    }

    /*---- Utility functions ----*/
    private var maxLogError = Double.NEGATIVE_INFINITY
    private fun log10RmsErr(
        xreal: DoubleArray,
        ximag: DoubleArray,
        yreal: DoubleArray,
        yimag: DoubleArray
    ): Double {
        val n = xreal.size
        require(n == ximag.size && n == yreal.size && n == yimag.size) { "Mismatched lengths" }
        var err = 10.0.pow((-99 * 2).toDouble())
        for (i in 0 until n) {
            val real = xreal[i] - yreal[i]
            val imag = ximag[i] - yimag[i]
            err += real * real + imag * imag
        }
        err = sqrt(err / max(n, 1)) // Now this is a root mean square (RMS) error
        err = log10(err)
        maxLogError = max(err, maxLogError)
        return err
    }

    private fun randomReals(size: Int): DoubleArray {
        val result = DoubleArray(size)
        for (i in result.indices) result[i] = random.nextDouble() * 2 - 1
        return result
    }

    private val random = Random()
}

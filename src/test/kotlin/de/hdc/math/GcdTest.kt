package de.hdc.math

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class GcdTest : FreeSpec ({
    "Should calculate GCD for list of two Intergers" {
        gcd(listOf(21L, 36L)) shouldBe 3L
    }

    "Should calculate GCD with three Intergers" {
        gcd(listOf(10L, 20L, 30L)) shouldBe 10L
    }

    "Should fail on empty list" {
        shouldThrow<IllegalArgumentException> {
            gcd(emptyList())
        }
    }
})

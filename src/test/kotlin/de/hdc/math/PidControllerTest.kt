package de.hdc.math

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class PidControllerTest : FreeSpec() {
    init {
        "proportional" {
            val pid = PidController(3.0, 0.0, 0.0, 100)
            pid.p shouldBe 3.0
            pid.i shouldBe 0.0
            pid.d shouldBe 0.0
            pid.tick(5.0, 2.0, 8.0) shouldBe 90.0
            pid.tick(5.0, 6.0, 8.0) shouldBe 30.0
            pid.tick(5.0, 9.0, 8.0) shouldBe -15.0
        }

        "derivative" {
            val pid = PidController(0.0, 0.0, 3.0, 100)
            pid.p shouldBe 0.0
            pid.i shouldBe 0.0
            pid.d shouldBe 3.0
            pid.tick(5.0, 2.0, 8.0) shouldBe -18.0
            pid.tick(5.0, 6.0, 8.0) shouldBe 12.0
            pid.tick(5.0, 6.0, 8.0) shouldBe 0.0
        }

        "integral" {
            val pid = PidController(0.0, 3.0, 0.0, 3)
            pid.p shouldBe 0.0
            pid.i shouldBe 3.0
            pid.d shouldBe 0.0
            pid.tick(5.0, 2.0, 8.0) shouldBe 90.0
            pid.tick(5.0, 4.0, 8.0) shouldBe 150.0
            pid.tick(5.0, 6.0, 8.0) shouldBe 180.0
            pid.tick(5.0, 7.0, 8.0) shouldBe 105.0
        }
    }
}

package de.hdc.math

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class LcmTest : FreeSpec ({
    "testLCM" {
        lcm(3, 4) shouldBe 12
        lcm(5, 3) shouldBe 15
        lcm(7, 5) shouldBe 35
        lcm(24, 18) shouldBe 72
        lcm(listOf(11567L, 12643L, 15871L, 19099L, 19637L, 21251L)) shouldBe 13133452426987L
    }
})

package de.hdc.math

import io.kotest.core.spec.style.*
import kotlinx.coroutines.*
import java.awt.*
import javax.swing.*

private class TestPane(private val points: List<Pair<Double, Double>>) : JPanel() {
    val WIDTH = 800
    val HEIGHT = 800
    val W2 = WIDTH / 2
    val H2 = HEIGHT / 2

    override fun getPreferredSize(): Dimension {
        return Dimension(WIDTH, HEIGHT)
    }

    fun transform(p: List<Pair<Double, Double>>): List<Pair<Int, Int>> =
        p.map { W2 + it.first.toInt() to H2 + it.second.toInt() }

    fun drawPoly(g: Graphics, p: List<Pair<Int, Int>>, closed: Boolean = true) {
        require(p.size > 1)
        for (i in 1 until p.size) {
            g.drawLine(p[i - 1].first, p[i - 1].second, p[i].first, p[i].second)
        }
        if (closed) {
            g.drawLine(p.last().first, p.last().second, p.first().first, p.first().second)
        }
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        g.color = Color.BLACK
        g.fillRect(0, 0, WIDTH, HEIGHT)
        g.color = Color.WHITE
        drawPoly(g, transform(points))
    }
}

private class CanvasWindow(name: String, private val pane: TestPane) {
    init {
        EventQueue.invokeLater {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
            JFrame(name).apply {
                defaultCloseOperation = JFrame.EXIT_ON_CLOSE
                layout = BorderLayout()
                add(pane)
                pack()
                setLocationRelativeTo(null)
                isVisible = true
            }
        }
    }

    fun repaint() {
        pane.repaint()
    }
}

class EggShapeTest : FreeSpec() {
    init {
        "draw some eggs" {
            val points1 = eggShell(
                400,
                400.0,
                300.0,
                0.0,
                200.0
            )
            val points2 = eggShell(
                400,
                400.0,
                300.0,
                40.0,
                180.0
            )
            val points3 = eggShell(
                400,
                400.0,
                300.0,
                60.0,
                160.0
            )
            val points4 = eggShell(
                400,
                400.0,
                400.0,
                0.0,
                330.0
            )
            val win = CanvasWindow("SuperShape2D", TestPane(points1 + points2 + points3 + points4))
            win.repaint()
            withContext(Dispatchers.IO) {
                Thread.sleep(1000)
            }
        }
    }
}

@file:Suppress("HardCodedStringLiteral")

package de.hdc.math

import io.kotest.assertions.throwables.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

@Suppress("EmptyRange")
class MapToTest : FreeSpec() {
    init {
        "zero length range float" {
            shouldThrow<IllegalArgumentException> {
                1.0f.mapTo(1.0f..1.0f, 3.0f..5.0f)
            }
        }

        "float identity" {
            1.0f.mapTo(1.0f..5.0f, 1.0f..5.0f) shouldBe 1.0f
            2.1f.mapTo(1.0f..5.0f, 1.0f..5.0f) shouldBe 2.1f
            5.0f.mapTo(1.0f..5.0f, 1.0f..5.0f) shouldBe 5.0f
        }

        "float transform" {
            1.0f.mapTo(1.0f..5.0f, 10.0f..50.0f) shouldBe 10.0f
            2.1f.mapTo(1.0f..5.0f, 10.0f..50.0f) shouldBe 21.0f
            5.0f.mapTo(1.0f..5.0f, 10.0f..50.0f) shouldBe 50.0f

            1.0f.mapTo(1.0f..5.0f, -10.0f..-50.0f) shouldBe -10.0f
            2.1f.mapTo(1.0f..5.0f, -10.0f..-50.0f) shouldBe -21.0f
            5.0f.mapTo(1.0f..5.0f, -10.0f..-50.0f) shouldBe -50.0f

            0.0f.mapTo(0.0f..5.0f, 0.0f..50.0f) shouldBe 0.0f
            2.1f.mapTo(0.0f..5.0f, 0.0f..50.0f) shouldBe 21.0f
            5.0f.mapTo(0.0f..5.0f, 0.0f..50.0f) shouldBe 50.0f

            1.0f.mapTo(1.0f..5.0f, 5.0f..1.0f) shouldBe 5.0f
            3.0f.mapTo(1.0f..5.0f, 5.0f..1.0f) shouldBe 3.0f
            5.0f.mapTo(1.0f..5.0f, 5.0f..1.0f) shouldBe 1.0f
        }

        "zero length range double" {
            shouldThrow<IllegalArgumentException> {
                1.0.mapTo(1.0..1.0, 3.0..5.0)
            }
        }

        "double identity" {
            1.0.mapTo(1.0..5.0, 1.0..5.0) shouldBe 1.0
            2.1.mapTo(1.0..5.0, 1.0..5.0) shouldBe 2.1
            5.0.mapTo(1.0..5.0, 1.0..5.0) shouldBe 5.0
        }

        "double transform" {
            1.0.mapTo(1.0..5.0, 10.0..50.0) shouldBe 10.0
            2.1.mapTo(1.0..5.0, 10.0..50.0) shouldBe 21.0
            5.0.mapTo(1.0..5.0, 10.0..50.0) shouldBe 50.0

            1.0.mapTo(1.0..5.0, -10.0..-50.0) shouldBe -10.0
            2.1.mapTo(1.0..5.0, -10.0..-50.0) shouldBe -21.0
            5.0.mapTo(1.0..5.0, -10.0..-50.0) shouldBe -50.0

            0.0.mapTo(0.0..5.0, 0.0..50.0) shouldBe 0.0
            2.1.mapTo(0.0..5.0, 0.0..50.0) shouldBe 21.0
            5.0.mapTo(0.0..5.0, 0.0..50.0) shouldBe 50.0

            1.0.mapTo(1.0..5.0, 5.0..1.0) shouldBe 5.0
            3.0.mapTo(1.0..5.0, 5.0..1.0) shouldBe 3.0
            5.0.mapTo(1.0..5.0, 5.0..1.0) shouldBe 1.0
        }

        "zero length range int" {
            shouldThrow<IllegalArgumentException> {
                1.mapTo(1..1, 3..5)
            }
        }

        "int identity" {
            1.mapTo(1..5, 1..5) shouldBe 1
            2.mapTo(1..5, 1..5) shouldBe 2
            5.mapTo(1..5, 1..5) shouldBe 5
        }

        "int transform" {
            1.mapTo(1..5, 10..50) shouldBe 10
            2.mapTo(1..5, 10..50) shouldBe 20
            5.mapTo(1..5, 10..50) shouldBe 50

            1.mapTo(1..5, -50..-10) shouldBe -50
            2.mapTo(1..5, -50..-10) shouldBe -40
            5.mapTo(1..5, -50..-10) shouldBe -10

            1.0.mapTo(1.0..5.0, -10.0..-50.0) shouldBe -10.0
            2.1.mapTo(1.0..5.0, -10.0..-50.0) shouldBe -21.0
            5.0.mapTo(1.0..5.0, -10.0..-50.0) shouldBe -50.0

            0.mapTo(0..5, 0..50) shouldBe 0
            2.mapTo(0..5, 0..50) shouldBe 20
            5.mapTo(0..5, 0..50) shouldBe 50

            0.mapTo(0..4, 0..10) shouldBe 0
            1.mapTo(0..4, 0..10) shouldBe 3
            2.mapTo(0..4, 0..10) shouldBe 5
            3.mapTo(0..4, 0..10) shouldBe 8
            4.mapTo(0..4, 0..10) shouldBe 10

            1.mapTo(1..5, -5..-1) shouldBe -5
            3.mapTo(1..5, -5..-1) shouldBe -3
            5.mapTo(1..5, -5..-1) shouldBe -1

            1.0.mapTo(1.0..5.0, 5.0..1.0) shouldBe 5.0
            3.0.mapTo(1.0..5.0, 5.0..1.0) shouldBe 3.0
            5.0.mapTo(1.0..5.0, 5.0..1.0) shouldBe 1.0
        }

        "zero length range long" {
            shouldThrow<IllegalArgumentException> {
                1L.mapTo(1L..1L, 3L..5L)
            }
        }

        "long identity" {
            1L.mapTo(1L..5L, 1L..5L) shouldBe 1L
            2L.mapTo(1L..5L, 1L..5L) shouldBe 2L
            5L.mapTo(1L..5L, 1L..5L) shouldBe 5L
        }

        "long transform" {
            1L.mapTo(1L..5L, 10L..50L) shouldBe 10L
            2L.mapTo(1L..5L, 10L..50L) shouldBe 20L
            5L.mapTo(1L..5L, 10L..50L) shouldBe 50L

            // Technically an empty Range. Works nonetheless.
            1L.mapTo(1L..5L, -10L..-50L) shouldBe -10L
            2L.mapTo(1L..5L, -10L..-50L) shouldBe -20L
            5L.mapTo(1L..5L, -10L..-50L) shouldBe -50L

            0L.mapTo(0L..5L, 0L..50L) shouldBe 0L
            2L.mapTo(0L..5L, 0L..50L) shouldBe 20L
            5L.mapTo(0L..5L, 0L..50L) shouldBe 50L

            0L.mapTo(0L..4L, 0L..10L) shouldBe 0L
            1L.mapTo(0L..4L, 0L..10L) shouldBe 3L
            2L.mapTo(0L..4L, 0L..10L) shouldBe 5L
            3L.mapTo(0L..4L, 0L..10L) shouldBe 8L
            4L.mapTo(0L..4L, 0L..10L) shouldBe 10L

            // Technically an empty Range. Works nonetheless.
            1L.mapTo(1L..5L, 5L..1L) shouldBe 5L
            3L.mapTo(1L..5L, 5L..1L) shouldBe 3L
            5L.mapTo(1L..5L, 5L..1L) shouldBe 1L
        }

        "zero length range bigdecimal" {
            shouldThrow<IllegalArgumentException> {
                "1".toBigDecimal().mapTo("1".toBigDecimal().."1".toBigDecimal(), "3".toBigDecimal().."5".toBigDecimal())
            }
        }

        "BigDecimal identity" {
            "1".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "1".toBigDecimal().."5".toBigDecimal()
            ) shouldBe "1".toBigDecimal()
            "2".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "1".toBigDecimal().."5".toBigDecimal()
            ) shouldBe "2".toBigDecimal()
            "5".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "1".toBigDecimal().."5".toBigDecimal()
            ) shouldBe "5".toBigDecimal()
        }

        "BigDecimal transform" {
            "1".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "10".toBigDecimal().."50".toBigDecimal()
            ) shouldBe "10".toBigDecimal()
            "2".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "10".toBigDecimal().."50".toBigDecimal()
            ) shouldBe "20".toBigDecimal()
            "5".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "10".toBigDecimal().."50".toBigDecimal()
            ) shouldBe "50".toBigDecimal()

            "1".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "-10".toBigDecimal().."-50".toBigDecimal()
            ) shouldBe "-10".toBigDecimal()
            "2".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "-10".toBigDecimal().."-50".toBigDecimal()
            ) shouldBe "-20".toBigDecimal()
            "5".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "-10".toBigDecimal().."-50".toBigDecimal()
            ) shouldBe "-50".toBigDecimal()

            "0".toBigDecimal().mapTo(
                "0".toBigDecimal().."5".toBigDecimal(),
                "0".toBigDecimal().."50".toBigDecimal()
            ) shouldBe "0".toBigDecimal()
            "2".toBigDecimal().mapTo(
                "0".toBigDecimal().."5".toBigDecimal(),
                "0".toBigDecimal().."50".toBigDecimal()
            ) shouldBe "20".toBigDecimal()
            "5".toBigDecimal().mapTo(
                "0".toBigDecimal().."5".toBigDecimal(),
                "0".toBigDecimal().."50".toBigDecimal()
            ) shouldBe "50".toBigDecimal()

            "0".toBigDecimal().mapTo(
                "0".toBigDecimal().."4".toBigDecimal(),
                "0".toBigDecimal().."10".toBigDecimal()
            ) shouldBe "0.0".toBigDecimal()
            "1".toBigDecimal().mapTo(
                "0".toBigDecimal().."4".toBigDecimal(),
                "0".toBigDecimal().."10".toBigDecimal()
            ) shouldBe "2.5".toBigDecimal()
            "2".toBigDecimal().mapTo(
                "0".toBigDecimal().."4".toBigDecimal(),
                "0".toBigDecimal().."10".toBigDecimal()
            ) shouldBe "5.0".toBigDecimal()
            "3".toBigDecimal().mapTo(
                "0".toBigDecimal().."4".toBigDecimal(),
                "0".toBigDecimal().."10".toBigDecimal()
            ) shouldBe "7.5".toBigDecimal()
            "4".toBigDecimal().mapTo(
                "0".toBigDecimal().."4".toBigDecimal(),
                "0".toBigDecimal().."10".toBigDecimal()
            ) shouldBe "10.0".toBigDecimal()

            "1".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "5".toBigDecimal().."1".toBigDecimal()
            ) shouldBe "5".toBigDecimal()
            "3".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "5".toBigDecimal().."1".toBigDecimal()
            ) shouldBe "3".toBigDecimal()
            "5".toBigDecimal().mapTo(
                "1".toBigDecimal().."5".toBigDecimal(),
                "5".toBigDecimal().."1".toBigDecimal()
            ) shouldBe "1".toBigDecimal()
        }
    }
}

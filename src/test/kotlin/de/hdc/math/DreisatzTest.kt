package de.hdc.math

import io.kotest.assertions.throwables.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class DreisatzTest : FreeSpec() {
    init {
        "func" {
            shouldThrow<IllegalArgumentException> {
                dreisatz(3, 240, null, null)
            }
            shouldThrow<IllegalArgumentException> {
                dreisatz(3, 240, 7, 560)
            }

            dreisatz(3, 240, 7, null) shouldBe 560
            dreisatz(3, 240, null, 560) shouldBe 7
            dreisatz(3, null, 7, 560) shouldBe 240
            dreisatz(null, 240, 7, 560) shouldBe 3
        }
    }
}

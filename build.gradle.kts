@file:Suppress("HardCodedStringLiteral")

plugins {
    kotlin("jvm") version "1.9.21"
    kotlin("kapt") version "1.9.21"
    `maven-publish`
}

kotlin {
    jvmToolchain(17)
    explicitApi()
}

group = "de.hdc.math"
version = "2023-12-10"

repositories {
    mavenCentral()
    mavenLocal()
}

apply {
    plugin("kotlin")
}

dependencies {
    testImplementation("io.kotest:kotest-runner-junit5:5.5.5")
}

tasks.withType<Test>().all {
    useJUnitPlatform()
}

//tasks.withType<Wrapper> {
//    distributionType = Wrapper.DistributionType.ALL
//    // with buildSrcVersions
//    gradleVersion = gradle.gradleVersion
//    // with refreshVersions
//    gradleVersion = gradle.gradleVersion
//}

private val sourcesJar: TaskProvider<Jar> by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets["main"].allSource)
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar.get())
        }
    }
}
